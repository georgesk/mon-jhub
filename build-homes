#! /usr/bin/python3

import ldap, sys, subprocess, os, glob, re

GROUP_MEMBER_RE = re.compile(r"CN=(.*),CN=.*,DC=lycee,DC=jb")

class KwartzLDAP:
    port = 1389
    host = "localhost"
    bindcn = "cn=personne,cn=users,dc=lycee,dc=jb"
    bindpw = "09071955"
    basedn = "cn=users,dc=lycee,dc=jb"
    searchScope = ldap.SCOPE_SUBTREE

    def __init__(self):
        self.l = ldap.initialize(f'ldap://{self.host}:{self.port}')
        try:
            self.l.protocol_version = ldap.VERSION3
            self.l.simple_bind_s(self.bindcn, self.bindpw)
            self.valid = True
        except Exception as error:
            print(error)
            self.valid = False
        return

    def __bool__(self):
        return self.valid

    def gidNumber2users(self, gidNumber):
        """
        Renvoie les utilisateurs d'un groupe, mais uniquement ceux dont c'est
        le groupe primaire d'appartenance
        :param:  gidNumber le numero du groupe
        :return: une liste de dictionnaires ; chaque dictionnaire
                 contient les propriété "cn", "sn", et "givenName"
                 d'un utilisateur.
        """
        searchFilter = f"(&(objectClass=person)(gidNumber={gidNumber}))"
        searchAttribute = ["cn", "givenName", "sn"]
        ldap_result_id = self.l.search(
            self.basedn, self.searchScope, searchFilter, searchAttribute)
        result_set = []
        while 1:
            result_type, result_data = self.l.result(ldap_result_id, 0)
            if (result_data == []):
                break
            else:
                if result_type == ldap.RES_SEARCH_ENTRY:
                    result_set.append(
                        {k:v[0].decode("utf-8")
                         for k, v in result_data[0][1].items()}
                    )
        return result_set

    def name2gidNumber(self, name):
        "transformation de nom de groupe en numéro"
        searchFilter = f"(&(objectClass=group)(cn={name}))"
        searchAttribute = ["gidNumber"]
        ldap_result_id = self.l.search(
            self.basedn, self.searchScope, searchFilter, searchAttribute)
        result_set = []
        while 1:
            result_type, result_data = self.l.result(ldap_result_id, 0)
            if (result_data == []):
                return -1
            else:
                if result_type == ldap.RES_SEARCH_ENTRY:
                    return int(
                        result_data[0][1]["gidNumber"][0].decode("utf-8"))

    def group2users(self, groupname, prof_aussi = False):
        """
        Renvoie les utilisateurs appartenant (ou invités à) un groupe
        :param: groupname le nom du groupe
        :param: prof_aussi décide si on inclut les profs dans la liste
                des utilisateurs à renvoyer
        :return: une liste de noms d'utilisateurs
        """
        searchFilter = f"(&(objectClass=group)(cn={groupname}))"
        searchAttribute = ["member"]
        ldap_result_id = self.l.search(
            self.basedn, self.searchScope, searchFilter, searchAttribute)
        all_users = []
        while 1:
            result_type, result_data = self.l.result(ldap_result_id, 0)
            if (result_data == []):
                break
            else:
                if result_type == ldap.RES_SEARCH_ENTRY:
                    all_users = [
                        GROUP_MEMBER_RE.match(m.decode("utf-8")).group(1) for
                        m in result_data[0][1]["member"]]
                    break
        if prof_aussi:
            return all_users
        return [ u for u in all_users if not self.est_prof(u)]

    def est_prof(self, user):
        """
        détermine si un utilisateur est un professeur
        """
        try:
            group = self.user2home(user).split("/")[2]
        except:
            return False
        return group.startswith("prof")
    
    def user2home(self, user):
        searchFilter = f"(&(objectClass=person)(cn={user}))"
        searchAttribute = ["unixHomeDirectory"]
        ldap_result_id = self.l.search(
            self.basedn, self.searchScope, searchFilter, searchAttribute)
        result_set = []
        while 1:
            result_type, result_data = self.l.result(ldap_result_id, 0)
            if (result_data == []):
                break
            else:
                if result_type == ldap.RES_SEARCH_ENTRY:
                    return result_data[0][1]["unixHomeDirectory"][0].decode("utf-8")
        return ""
    
    def ensureHome(self,user):
        path = self.user2home(user)
        group = os.path.basename(os.path.dirname(path))
        if not os.path.isdir(path):
            subprocess.run(["mkdir", "-p", path])
            subprocess.run(["chown", f"{user}:{group}", path])
            subprocess.run(["chmod", "700", path])
        for shared_dir in glob.glob("/srv/nbgrader/exchange/__*"):
            dirname = os.path.basename(shared_dir)
            if not os.path.islink(os.path.join(path, dirname)):
                subprocess.call(["rm", "-rf", dirname])
                subprocess.call([
                    "ln", "-s", f"/srv/nbgrader/exchange/{dirname}", path])
        return path

STUDENT_CONF = """\
c = get_config()
c.CourseDirectory.course_id = "cours-{group}"
"""
PROF_CONF = STUDENT_CONF + """\
c.ClearSolutions.code_stub = {{
    "python": "# écrivez votre code ici\\nraise NotImplementedError",
}}
c.ClearSolutions.text_stub = \"\"\"\\
# DOUBLE-CLIQUEZ ICI, ET ...
# REMPLACEZ CE TEXTE PAR VOTRE RÉPONSE, S.V.P.\"\"\"
"""


def create_nbgrader_config(path, which, course_id):
    """
    Create the file nbgrader_config in some path
    @param path a directory
    @param which STUDENT_CONF or PROF_CONF
    @param course_id the id of the course
    """
    NBC = os.path.join(path, "nbgrader_config.py")
    if not os.path.isfile(NBC):
        with open(NBC,"w") as nb_config:
            nb_config.write(which.format(group=course_id))
            print(f"créé {NBC}")
    return
    
def run():
    ## seul fichier à prendre en compte : profs_cours.txt
    kw = KwartzLDAP()
    if not os.path.exists("profs_cours.txt"):
        return
    pg = [l.strip().split(":") for l in open("profs_cours.txt").readlines()
      if ":" in l and not l.startswith("#")]
    for prof, group in pg:
        print(f"{prof} => {group}")
        # create home directory for the prof
        path = kw.ensureHome(prof)
        create_nbgrader_config(path, PROF_CONF, group)
        # create home directories for the students of the group
        users = kw.group2users(group)
        for u in users:
            path = kw.ensureHome(u)
            create_nbgrader_config(path, STUDENT_CONF, group)
    ## permet l'usage d'un shell pour les utilisateurs
    subprocess.call(["ln", "-sf", "/bin/bash", "/bin/kwartz-sh"])
    return

if __name__ == "__main__":
    run()
    
