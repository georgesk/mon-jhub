#! /bin/sh

(ssh ${REMOTE_USER}@${REMOTE_HOST} -N -L ${LOCAL_LDAP_PORT}:localhost:${REMOTE_LDAP_PORT} &)
service nslcd start
(cd /usr/local/lib/python3.9/site-packages/notebook/templates && patch -p2 < /code/notebook-tree.diff)
./build-homes
PYTHONPATH=/usr/lib/python3/dist-packages jupyterhub
