#! /usr/bin/python3

"""
Le but de ce fichier est de gérer facilement l'ajout de paires
(prof, cours)

prof
====
C'est un nom de compte, qui appartient au group "profs", par exemple,
"georges.khaznadar".

cours
=====
C'est un nom de cours, il est basé sur un nom de groupe d'élèves. Par exemple
"cours_c2d08" est le cours destinée au group "c2d08", qui est « la seconde 8 ».
"""

import sys, os
from datetime import datetime

from jh_config import ConfigWizard, ProfError, GroupError

IN_CONF_FILE = "jupyterhub_config.py.orig"
OUT_CONF_FILE = "jupyterhub_config.py"
# le nom qui contient les configurations sans cours

if __name__ == "__main__":
    if not os.path.exists("profs_cours.txt"):
        print("Il faut un fichier profs_cours.txt, avec un prof et un cours par ligne,\nséparés par un caractère « : »")
        sys.exit(1)
    cw = ConfigWizard(open(IN_CONF_FILE).readlines())
    pg = [l.strip().split(":") for l in open("profs_cours.txt").readlines()
          if ":" in l and not l.startswith("#")]
    for i, (prof, group) in enumerate(pg):
        try:
            cw.newTeaching(prof, group,
                           context = f"profs_cours.txt, ligne {i+1}")
            print(f"Nouvelle configuration : {prof} => {group}")
        except ProfError as err:
            print(err.message); sys.exit(1)
        except GroupError as err:
            print(err.message); sys.exit(1)
    timestamp = datetime.now().strftime("%Y-%m-%d:%H:%M:%S")
    backup = f"bak-{timestamp}-{OUT_CONF_FILE}"
    cmd = f"cp {OUT_CONF_FILE} {backup}"
    os.system(cmd)
    with open(OUT_CONF_FILE, "w") as writable_conf_file:
        writable_conf_file.write(str(cw))
    print(f"L'ancien fichier est sauvegardé sous le nom « {backup} »")

