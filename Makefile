# scripts pour maintenir l'environnement de développement

all:

build:
	docker build -t my_jhub:1 --network=host .

mrpropre: stopserver
	# nettoye le répertoire exchange, sauf ce qui commence par "_"
	sudo rm -rf $$(ls -d exchange/[a-zA-Z0-9]*)
	sudo rm -rf home/*
	rm -f jupyterhub.sqlite
	: > nohup.out

startserver:
	./manageProfCours.py # assoc. prof_cours dans jupytherhub_config.py
	(nohup docker-compose up web &)

stopserver:
	docker-compose down

startshell:
	./manageProfCours.py # assoc. prof_cours dans jupytherhub_config.py
	docker-compose run web bash

publish_rsync:
	rsync -av --files-from=synchro.txt . lyceejeanbart.fr:developpement/docker/jupyterhub/

.PHONY: mrpropre startserver stopserver startshell publish_rsync
