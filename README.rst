UN SERVICE JUPYTERHUB POUR LE LYCÉE JEAN BART
#############################################

Sur ce dépôt, on trouvera des matériaux et du code, ceux qui ont permis le
déployement du service web https://python.lyceejeanbart.fr, basé sur les
logiciels libres `docker <https://www.docker.com/>`_,
`jupyterhub <https://jupyter.org/hub>`_
et son extension `nbgrader <https://github.com/jupyter/nbgrader>`_,
qui facilite la gestion de devoirs.

Les membres de la communauté éducative du lycée Jean Bart s'authentifient
sur ce service en utilisant leurs accréditations ordinaires, en effet
les comptes sont gérés par le système
`PAM/LDAP <https://fr.wikipedia.org/wiki/Pam_ldap>`_, et l'annuaire LDAP
utilisé est celui qui procure l'authentification ordinaire dans le
`réseau pédagogique du lycée <https://kwartz.lyceejeanbart.fr>`_.

.. image:: jb_jh_login.png
    :width: 300px
    :align: right
    :alt: écran d'authentification

