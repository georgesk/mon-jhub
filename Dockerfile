FROM python:3
WORKDIR /code

ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update &&\
 apt-get install --yes less npm nano emacs-nox nslcd ldap-utils rsync\
 python3-ldap python3-docutils python3-markdown python3-matplotlib\
 python3-mdx-math python3-networkx python3-numpy python3-opencv\
 python3-pandas python3-pep8 python3-pil python3-pip python3-pygments\
 python3-pygraphviz python3-requests python3-stemmer python3-sympy \
 python3-pil \
 && rm -rf /var/cache/apt/archives

RUN npm install -g configurable-http-proxy && npm cache clean --force &&\
 mkdir /github && cd /github && \
 git clone https://github.com/jupyterhub/jupyterhub.git &&\
 cd jupyterhub && pip install -e . &&\
 cd /github && \
 git clone https://github.com/jupyter/nbconvert.git &&\
 cd nbconvert && pip install -e . &&\
 cd /github && \
 git clone https://github.com/jupyter/nbgrader.git &&\
 cd nbgrader && pip install -e . &&\
 pip install  folium && pip install mobilechelonian

# this port is featured by jupyterhub
EXPOSE 8000

# copy customized files for LDAP/PAM authentication
# with a service at port 1389, based on LDAP service provided
# by Lycée Jean Bart
COPY dirs_to_build .
RUN rsync -a etc/* /etc

# install and enable nbgrader extensions
RUN jupyter nbextension install --sys-prefix --py nbgrader --overwrite && \
    jupyter nbextension enable --sys-prefix --py nbgrader && \
    jupyter serverextension enable --sys-prefix --py nbgrader

CMD ["jupyterhub"]
