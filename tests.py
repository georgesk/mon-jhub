#! /usr/bin/python3

import unittest, re
from io import StringIO

from jh_config import ConfigWizard

CONF_FILE = "jupyterhub_config.py_for_tests"
# le nom qui contient les configurations

LOREM = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed non risus. Suspendisse lectus tortor, dignissim sit amet, adipiscing nec, ultricies sed, dolor. Cras elementum ultrices diam. Maecenas ligula massa, varius a, semper congue, euismod non, mi. Proin porttitor, orci nec nonummy molestie, enim est eleifend mi, non fermentum diam nisl sit amet erat."

class TestManageProfCours(unittest.TestCase):

    def test_01_groupe_lignes(self):
        with open(CONF_FILE) as infile:
            cw = ConfigWizard(infile.readlines())
            re1 = re.compile(r"^# Configuration file for jupyterhub.*")
            re2 = re.compile(r"^##.*")
            debut, lignes = cw.groupe_lignes(re1, re2)
            assert(debut == 0 and len(lignes) == 6)
        return

    def test_02_get_property(self):
        with open(CONF_FILE) as conf_file:
            cw = ConfigWizard(conf_file.readlines())
            val, debut, lignes = cw.get_property("this_is_for_the_test_suite")
            assert(val == "OK, everything is fine")
        return

    def test_02a_get_property(self):
        """
        même test, mais avec une propriété initialement désactivée
        """
        with open(CONF_FILE) as conf_file:
            cw = ConfigWizard(conf_file.readlines())
            val, debut, lignes = cw.get_property(
                "this_is_for_the_test_suite_also")
            assert(val == "This property is commented out")
        return

    def test_03_set_property(self):
        new_value = {
            "text": LOREM,
            "float_val": 3.14,
        }
        with open(CONF_FILE) as conf_file:
            cw = ConfigWizard(conf_file.readlines())
            cw.set_property(
                "this_is_for_the_test_suite", new_value)
            val, debut, lignes = cw.get_property("this_is_for_the_test_suite")
            assert (val == new_value)
            lengths = [len(re.sub("\n$", "", l)) for l in lignes]
            assert(max(lengths) <= 72)
        return

    def test_04_change_in_property(self):
        new_value = {
            "text": LOREM,
            "float_val": 3.14,
        }
        with open(CONF_FILE) as conf_file:
            cw = ConfigWizard(conf_file.readlines())
            cw.set_property(
                "this_is_for_the_test_suite", new_value)
            cw.change_in_property(
                "this_is_for_the_test_suite", "int_val", 100)
            new_val, debut, lignes = cw.get_property(
                "this_is_for_the_test_suite")
            assert(new_val["int_val"] == 100)
        return
        
    def test_05_append_to_property(self):
        new_value = LOREM.split(" ")
        with open(CONF_FILE) as conf_file:
            cw = ConfigWizard(conf_file.readlines())
            cw.set_property(
                "this_is_for_the_test_suite", new_value)
            cw.append_to_property(
                "this_is_for_the_test_suite", "Hello_world")
            new_val2, debut, lignes = cw.get_property(
                "this_is_for_the_test_suite")
            assert(len(new_val2) == len(new_value)+1)
            assert("Hello_world" in new_val2)
            cw.append_to_property(
                "Authenticator.allowed_users", "benoit.markey")
            new_set, debut, lignes = cw.get_property(
                "Authenticator.allowed_users")
            assert(isinstance(new_set, set) and "benoit.markey" in new_set)
        return
        
    def test_06_modify_property(self):
        new_value = {
            "text": LOREM,
            "float_val": 3.14,
        }
        with open(CONF_FILE) as conf_file:
            #### test the "set" mode
            cw = ConfigWizard(conf_file.readlines())
            cw.modify_property(
                "set", "this_is_for_the_test_suite", new_value)
            val, debut, lignes = cw.get_property("this_is_for_the_test_suite")
            assert (val == new_value)
            lengths = [len(re.sub(r"\n$", "", l)) for l in lignes]
            assert(max(lengths) <= 72)
            #### test the "append_to" mode
            new_value = LOREM.split(" ")
            cw.set_property(
                "this_is_for_the_test_suite", new_value)
            cw.modify_property(
                "append_to", "this_is_for_the_test_suite", "Hello_world")
            new_val2, debut, lignes = cw.get_property(
                "this_is_for_the_test_suite")
            assert(len(new_val2) == len(new_value)+1)
            assert("Hello_world" in new_val2)
            #### test the "key_val" mode
            new_value = {
                "text": LOREM,
                "float_val": 3.14,
            }
            cw.set_property(
                "this_is_for_the_test_suite", new_value)
            cw.modify_property(
                "key_val", "this_is_for_the_test_suite", "int_val", 100)
            new_val, debut, lignes = cw.get_property(
                "this_is_for_the_test_suite")
            assert(new_val["int_val"] == 100)
            lengths = [len(re.sub(r"\n$", "", l)) for l in lignes]
            assert(max(lengths) <= 72)
        return

    def test_07_newTeaching(self):
        with open(CONF_FILE) as conf_file:
            cw = ConfigWizard(conf_file.readlines())
            prof = "benoit.markey"
            group = "c2d01"
            cw.newTeaching(prof, group)
            load_groups, debut, lignes = cw.get_property("JupyterHub.load_groups")
            assert(prof in load_groups[f"formgrade-cours-{group}"])
            assert(load_groups[f"nbgrader-cours-{group}"] == [])
            lengths = [len(re.sub(r"\n$", "", l)) for l in lignes]
            assert(max(lengths) <= 72)
            services, debut, lignes = cw.get_property("JupyterHub.services")
            services1 = [s for s in services if s["name"] == f"cours-{group}"]
            assert(len(services1) == 1)
            s = services1[0]
            lengths = [len(re.sub(r"\n$", "", l)) for l in lignes]
            assert(max(lengths) <= 72)
            # there is a service with port 1234 in the file
            # jupyterhub_config.py_for_tests
            assert(s["url"] == "http://127.0.0.1:1233")
            assert(s["command"][1] == f"--group=formgrade-cours-{group}")
            assert(s["user"] == prof)
            assert(s["cwd"] == ConfigWizard.homeProf(prof))
            urls = [s["url"] for s in services]
            # verify that the list of urls has no redundancy
            assert(len(urls) == len(set(urls)))
            return

if __name__ == '__main__':
    unittest.main()
