### fichier jh_config.py ###

import re, pprint
from io import StringIO
from subprocess import run

class ProfError(Exception):
    def __init__(self, prof, message=None, context=""):
        if context:
            context = f" ({context})"
        self.expression = prof
        if message:
            self.message = message
        else:
            self.message = f"ERREUR{context} : « {prof} » n'est pas un compte professeur"
        return
    
class GroupError(Exception):
    def __init__(self, group, message=None, context=""):
        if context:
            context = f" ({context})"
        self.expression = group
        if message:
            self.message = message
        else:
            self.message = f"ERREUR{context} : le groupe {group} n'existe pas"
        return
    

class ConfigWizard:
    """
    Une classe pour faire de la magie dans le fichier de configuration
    jupyterhub_config.py, étant entendu les conventions suivantes :

      1. les propriétés à changer sont des propriétés d'un objet `c`
      2. quand on rencontre "##" en début de ligne, ça veut dire qu'on
         entre dans le domaine de la documentation d'une nouvelle
         propriété
      3. dans un premier temps, la magie ne permet pas de toucher une
         propriété qui serait encore contenue dans un commentaire pur ;
         elle doit avoir été décommentée au préalable
      4. Les 7 premières lignes du fichier jupyterhub_config.py sont
         restées sans modification (telles que faites par la commande
         `jupyterhub --generate-config`), pour la suite de tests
      5. le fichier contient quelque part cette ligne, pour la suite de
         tests :
         c.this_is_for_the_test_suite = "OK, everything is fine"

    Paramètres du constructeur :
    ============================

    :param: lines les lignes du fichiers de configuration (avec leurs
            caractères de fin de ligne)
    """

    def __init__ (self, lines):
        self.lines = lines
        return

    def groupe_lignes(self, re1, re2):
        """
        Trouve les lignes entre deux marqueurs
        :param: re1 une expression régulière qui permet de trouver la première ligne
        :type : re.Pattern
        :param: re2 une expression régulière qui permet de trouver la dernière ligne
                (c'est à dire juste après la dernière ligne)
        :type : re.Pattern
        :result: le numéro de la ligne de début et la liste des lignes reconnues
        :rtype : int ou None, [str, ...]
        """
        debut = fin = None
        r_lignes = []
        for i, l in enumerate(self.lines):
            if re1.match(l):
                debut = i
                r_lignes.append(l)
                break
        if debut == None:
            # deuxième chance : et si la première ligne est juste commentée
            for i, l in enumerate(self.lines):
                if l[0] == "#" and re1.match(l[1:]):
                    debut = i
                    r_lignes.append(l[1:])
                    break
        if debut == None:
            return None, []
        for i, l in enumerate(self.lines[debut+1:]):
            if re2.match(l):
                break
            else:
                r_lignes.append(l)
        return debut, r_lignes

    def get_property(self, name):
        """
        trouve dans le fichier de configuration la valeur d'une propriété
        par exemple si `name` vaut "JupyterHub.load_groups", ça cherche
        la valeur de c.JupyterHub.load_groups et ça renvoie cette valeur,
        plus la liste des lignes du fichier de configuration où se trouve
        celle valeur
        :param: name nom de la propriété
        :type : str
        :return: valeur ou None, ligne de début et liste de lignes
        :rtype : objet Python, int, [str, ...]
        """
        debut, lignes = self.groupe_lignes(
            re.compile(r"c\."+name),
            re.compile("^##")
        )
        if debut == None or not lignes:
            return None, None, []
        expression = [re.sub("c."+name+r"\s*=\s*", "", lignes[0])] + \
                lignes[1:]
        val = eval("".join(expression))
        return val, debut, lignes

    def set_property(self, name, value):
        """
        change a property in the configuration; 
        side-effect : modifies self.lines
        :param: name a property name for the object c; for example, 
                "Application.log_level" for `c.Application.log_level`
        :type : str
        :param: value the new value for this property
        :type : any Python object
        """
        debut, lignes = self.groupe_lignes(
            re.compile("c."+name),
            re.compile("^##")
        )
        if debut == None or not lignes:
            return
        new_lines = self.lines[:debut]
        pp = pprint.PrettyPrinter(indent=4, width = 72, compact = True)
        expression = f"c.{name} = \\\n{pp.pformat(value)}\n\n"
        new_lines += [l + "\n" for l in expression.split("\n")]
        new_lines += self.lines[debut+len(lignes):]
        self.lines = new_lines
        return

    def append_to_property(self, name, value):
        """
        appends a value to a list-like property in a configuration file
        side-effect: modifies self.lines
        :param: name a property name for the object c; for example, 
                "Application.log_level" for `c.Application.log_level`
        :type : str
        :param: value the new value for this property
        :type : any Python object
        """
        the_list, debut, lignes = self.get_property(name)
        if debut == None or not lignes:
            return
        new_lines = self.lines[:debut]
        if isinstance(the_list, set):
            the_list.add(value)
        else:
            the_list.append(value)
        pp = pprint.PrettyPrinter(indent=4, width = 72, compact = True)
        expression = f"c.{name} = \\\n{pp.pformat(the_list)}\n\n"
        new_lines += [l + "\n" for l in expression.split("\n")]
        new_lines += self.lines[debut+len(lignes):]
        self.lines = new_lines
        return

    def change_in_property(self, name, key, value):
        """
        appends a value to a dictionary-like property in a configuration file
        side-effect: modifies self.lines
        :param: name a property name for the object c; for example, 
                "Application.log_level" for `c.Application.log_level`
        :type : str
        :param: key a key for the dictionary
        :type : any scalar
        :param: value the new value for this property
        :type : any Python object
        """
        the_dict, debut, lignes = self.get_property(name)
        if debut == None or not lignes:
            return
        new_lines = self.lines[:debut]
        the_dict[key] = value
        pp = pprint.PrettyPrinter(indent=4, width = 72, compact = True)
        expression = f"c.{name} = \\\n{pp.pformat(the_dict)}\n\n"
        new_lines += [l + "\n" for l in expression.split("\n")]
        new_lines += self.lines[debut+len(lignes):]
        self.lines = new_lines
        return
        
    def modify_property(self, how, *args):
        """
        Performs a modification in a configuration file
        side-effect: modifies self.lines
        :param: how a switch, which can be "set", "append_to", "key_val"
        :type : str
        :param: *args a list of positional parameters, which depends on
                the wanted modification : a single value for "set" and
                "append_to", and a key,value pair for "key_val".
        """
        methods = {
            "set": self.set_property,
            "append_to": self.append_to_property,
            "key_val": self.change_in_property,
        }
        methods[how](*args)
        return

    def __str__ (self):
        return "".join(self.lines)

    @staticmethod
    def checkGroup(group):
        """
        Vérifie qu'on a bien affaire à un groupe existant, avec ou sans
           un caractère "c" en préfixe
        @param group le nom d'un groupe
        @return le nom du groupe, éventuellement préfixé d'un "c" si le
           groupe existe, sinon une chaîne vide
        """
        cp = run (["getent", "group", group], capture_output=True)
        reply = cp.stdout.decode("utf-8").strip()
        if not reply:
            group = "c"+group
            cp = run (["getent", "group", group], capture_output=True)
            reply = cp.stdout.decode("utf-8").strip()
            if not reply:
                return ""
        return group

    @staticmethod
    def homeProf(prof):
        """
        Renvoie le chemin vers le dossier personnel d'un prof
        @param prof un nom de compte
        @return une chaîne vide si le compte n'est pas celui d'un prof, sinon
                le chemin vers son dossier personnel
        """
        cp = run (["getent", "passwd", prof], capture_output=True)
        reply = cp.stdout.decode("utf-8").strip()
        if not reply:
            return ""
        gid = reply.split(":")[3]
        cp = run (["getent", "group", gid], capture_output=True)
        reply = cp.stdout.decode("utf-8").strip()
        if not reply:
            return ""
        group = reply.split(":")[0]
        if not group.lower().startswith("prof"):
            return ""
        return f"/home/{group}/{prof}"

    def newTeaching(self, prof, group, context=""):
        """
        binds a teacher to a sudents' groups and returns a new configiration
        side-effect : modifies self.lines
        :param prof: this is a user name, whose group name must start with
                     "prof"
        :type  prof: str
        :param group: it is the name of a students' group. If one provides
                      a name like "c2d01", the course will be named
                      "cours-c2d01"
        :type  group: str
        :param context: a message useful to triage errors
        :type  context: str
        """
        home = self.homeProf(prof)
        if not home:
            raise ProfError(prof, context=context)
        if not self.checkGroup(group):
            raise GroupError(group, context=context)
        ## modification of c.JupyterHub.load_groups ##
        lg, debut, fin = self.get_property("JupyterHub.load_groups")
        if 'formgrade-'+group in lg:
            liste_profs = lg['formgrade-'+group] + [prof]
        else:
            liste_profs = [prof]
        self.modify_property("key_val", "JupyterHub.load_groups",
                             'formgrade-cours-'+group, liste_profs)
        self.modify_property("key_val", "JupyterHub.load_groups",
                                     'nbgrader-cours-'+group, [])
        ## modification of c.JupyterHub.services ##
        services, debut, lignes = self.get_property("JupyterHub.services")
        ports = [int((s["url"].split(":"))[2]) for s in services]
        ## port 9999 is used for jupyterhub its self, but numbers
        ## below 9999 can be used
        new_port = 9998
        if ports:
            new_port = min(ports) - 1
        new_service = {
            'name': f'cours-{group}',
            'url': f'http://127.0.0.1:{new_port}',
            'command': [
                'jupyterhub-singleuser',
                f'--group=formgrade-cours-{group}',
                '--debug',
            ],
            'user': f'{prof}',
            'cwd': f'{self.homeProf(prof)}',
            'api_token': '5ce9988168bd45a78f889640e56ae70c'
        }
        self.modify_property("append_to", "JupyterHub.services",
                                      new_service)
        ## let us not use Authenticator.allowed_users since it prevents
        ## PAM/LDAP to do the job when a new users connects
        # self.modify_property("append_to", "Authenticator.allowed_users", prof)
        return
